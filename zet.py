#!/usr/bin/env python
# zet.py Zettelkasten Manager
# Requires fzf for some options 
# Missing:
# -e, --export  export your notes
import os
import subprocess
from datetime import datetime
import argparse
import sys

editor = "emacs"
extension = "org"
document_start = """#+TITLE: %TITLE%
#+OPTIONS: toc:nil num:nil 

"""

parser = argparse.ArgumentParser(description='zet.py Zettelkasten Manager.')
parser.add_argument('-t', '--titles', action="store_true", default=False, help='list all titles of your notes')
parser.add_argument('-n', '--new', action="store_true", default=False, help='create a new note')
parser.add_argument('-d', '--delete', action="store_true", default=False, help='delete a note')
parser.add_argument('-o', '--open', action="store_true", default=False, help='open a note')
parser.add_argument('-l', '--last', action="store_true", default=False, help='print your last note')
args = parser.parse_args()

data = []
def list_files():
    for path, subdirs, files in os.walk(str(os.getcwd())):
        for name in files:
            if name.endswith(extension):
                with open(os.path.join(path,name)) as f:
                    title = ' '.join(f.read().splitlines()[0].split(" ")[1:])
                    data.append(f"[{os.path.basename(path)}] {title}")
if args.new:
    date = datetime.now().strftime("%Y%m%d%H%M%S")    
    os.mkdir(date)
    title = input("Temporary title for the new zet: ")
    file_location = os.path.join(os.getcwd(),date,f"README.{extension}")
    with open(file_location,"w") as f:
        f.write(document_start.replace("%TITLE%",title))
    subprocess.Popen([editor, file_location])
elif args.titles:
    list_files()
    for title in data:
        print(title)
elif args.open:
    list_files()
    data = '\n'.join(data)
    with open("for_fzf_data.txt","w") as f:
        f.write(data)
    choice = subprocess.getoutput("fzf --reverse < for_fzf_data.txt")
    os.remove("for_fzf_data.txt")
    if choice != "":
        choice = choice.split("]")[0].replace("[","")
        cwd = os.getcwd()
        subprocess.Popen([editor, os.path.join(cwd,choice,'README.org')])
elif args.delete:
    list_files()
    data = '\n'.join(data)
    with open("for_fzf_data.txt","w") as f:
        f.write(data)
    choice = subprocess.getoutput("fzf --reverse < for_fzf_data.txt")
    os.remove("for_fzf_data.txt")
    if choice != "":
        choice = choice.split("]")[0].replace("[","")
        cwd = os.getcwd()
        os.remove(os.path.join(cwd,choice,'README.org'))
        os.rmdir(os.path.join(cwd,choice))
elif args.last:
    list_files()
    print(data[-1])
