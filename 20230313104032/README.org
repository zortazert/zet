#+TITLE: What is Abstraction?
#+OPTIONS: toc:nil num:nil 

One way to think of abstraction is hiding the inner workings of something and making only the relevant information available to the outside of something like a class or a function. It is simplifying the code for the human brain. You don't need to know how a class/function/library works, you just need to know what to give it and what will it return. The inner workings are abstracted.

A team of programmers writes libraries of functions and classes and then another team actually uses them. The team that ueses them will never edit the code that makes up the libraries and don't need to understand how they work. For them the code is abstracted. For the team who make the libraries the code should be documented in order to change it, update it and replace it. 
