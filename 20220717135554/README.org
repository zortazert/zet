#+TITLE: Download github wiki
#+OPTIONS: toc:nil num:nil 

Many projects on github use a wiki section in order to handle documentation. This can be downloaded/archived apperently by doing:

#+BEGIN_SRC bash
git clone https://github.com/USER/REPO.wiki.git
#+END_SRC
