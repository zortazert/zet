#+TITLE: Open file manager in cmd
#+OPTIONS: toc:nil num:nil 

#+BEGIN_SRC cmd
start . 
#+END_SRC

* Referenced:
https://www.howtogeek.com/691201/how-to-open-file-explorer-using-command-prompt-on-windows-10/
